/*
 * Test for using MicroStrain 3DM-GX3-45 with RT CDC ACM driver
 *
 * Copyright (c) Siemens AG, 2012, 2013
 * Authors:
 *  Jan Kiszka <jan.kiszka@siemens.com>
 *
 * This work is licensed under the terms of the GNU GPL, version 2.  See
 * the COPYING file in the top-level directory.
 */

#include <assert.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <termio.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/mman.h>

#ifdef __XENO__
#include <rtdm/rtserial.h>
#endif

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

struct mip_header {
	uint8_t sync[2];
	uint8_t descriptor_set;
	uint8_t payload_len;
} __attribute__((packed));

struct field_header {
	uint8_t len;
	uint8_t descriptor;
} __attribute__((packed));

struct mip_packet {
	struct mip_header mip_header;
	struct field_header field_header;
	uint8_t field_data[];
} __attribute__((packed));

#define CMDSET_BASE			0x01
#define CMDSET_3DM			0x0c

#define CMD_PING			0x01
#define  CMD_PING_LEN			2
#define CMD_IDLE			0x02
#define  CMD_IDLE_LEN			2
#define CMD_DEVINFO			0x03
#define  CMD_DEVINFO_LEN		2
#define RPLY_ACK_NACK			0xf1
#define  RPLY_ACK_NACK_LEN		4

#define CMD_AHRSFMT			0x08
#define  CMD_AHRSFMT_LEN		13
#define CMD_CONTMODE			0x11
#define  CMD_CONTMODE_LEN		5

static uint32_t read_be_u32(uint8_t *buf)
{
	return (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3];
}

static float read_sp_ieee754(uint8_t *buf)
{
	union {
		uint32_t binary_val;
		float float_val;
	} convert;

	convert.binary_val = read_be_u32(buf);
	return convert.float_val;
}

static uint16_t calc_crc(uint8_t *data, size_t len)
{
	uint8_t c1 = 0, c2 = 0;

	while (len-- > 0) {
		c1 += *data++;
		c2 += c1;
	}
	return ((uint16_t)c1 << 8) + c2;
}

static void mip_packet_send(int fd, struct mip_packet *packet, uint8_t set,
			    uint8_t descriptor, uint8_t len, uint8_t *data)
{
	size_t packet_len, data_len;
	uint16_t crc;
	int res;

	packet->mip_header.sync[0] = 0x75;
	packet->mip_header.sync[1] = 0x65;
	packet->mip_header.descriptor_set = set;
	packet->mip_header.payload_len = len;
	packet->field_header.len = len;
	packet->field_header.descriptor = descriptor;
	data_len = len - sizeof(struct field_header);
	memcpy(packet->field_data, data, data_len);

	packet_len = sizeof(struct mip_header) + len;
	crc = calc_crc((uint8_t *)packet, packet_len);
	packet->field_data[data_len] = crc >> 8;
	packet->field_data[data_len + 1] = crc;

	packet_len += 2;
	res = write(fd, packet, packet_len);
	assert(res == packet_len);
}

static void mip_packet_receive(int fd, struct mip_packet *packet,
			       size_t buffer_size)
{
	size_t packet_len;
	uint16_t crc;
	uint8_t *c;
	int res;

	res = read(fd, &packet->mip_header, sizeof(packet->mip_header));
	assert(res == sizeof(packet->mip_header));

	packet_len = sizeof(struct mip_header) +
		packet->mip_header.payload_len + 2;
	assert(packet_len <= buffer_size);

	res = read(fd, &packet->field_header,
		   packet->mip_header.payload_len + 2);
	assert(res == packet->mip_header.payload_len + 2);

	c = ((uint8_t *)packet) + packet_len - 2;
	crc = calc_crc((uint8_t *)packet, packet_len - 2);
	assert(((uint16_t)c[0] << 8) + c[1] == crc);
}

static bool mip_packet_acked(struct mip_packet *packet)
{
	return packet->mip_header.payload_len >= RPLY_ACK_NACK_LEN &&
		packet->field_header.len == RPLY_ACK_NACK_LEN &&
		packet->field_header.descriptor == RPLY_ACK_NACK &&
		packet->field_data[1] == 0;
}

int main(int argc, char *argv[])
{
	static uint8_t ahrs_format[] = {
		0x01, 0x03,
		0x04, 0x00, 0x01,
		0x05, 0x00, 0x01,
		0x12, 0x00, 0x01
	};
	static uint8_t ahrs_save[] = {
		0x03, 0x00
	};
	static uint8_t ahrs_enable_cont[] = {
		0x01, 0x01, 0x01
	};
	union {
		struct mip_packet packet;
		uint8_t space[1024];
	} buffer;
	struct mip_packet *packet = &buffer.packet;
	struct timespec last, now;
	unsigned long long delta;
	float raw_value[3];
	char str[17];
	int fd, res, i;
#ifdef __XENO__
	struct rtser_config config;
#endif
	struct sched_param param = { .sched_priority = 1 };

	res = mlockall(MCL_CURRENT | MCL_FUTURE);
	assert(res == 0);

	res = pthread_setschedparam(pthread_self(), SCHED_FIFO, &param);
	assert(res == 0);

	if (argc < 2) {
		printf("%s <device-name>\n", argv[0]);
		return 1;
	}
	fd = open(argv[1], O_RDWR);
	assert(fd >= 0);

	mip_packet_send(fd, packet, CMDSET_BASE, CMD_IDLE, CMD_IDLE_LEN, NULL);

#ifdef __XENO__
	config.config_mask = RTSER_SET_TIMEOUT_RX;
	config.rx_timeout = RTSER_TIMEOUT_NONE;
	res = ioctl(fd, RTSER_RTIOC_SET_CONFIG, &config);
	assert(res == 0);

	i = 100;
	while (--i > 0) {
		nanosleep(&(struct timespec){ 0, 1000000 }, NULL);
		while (read(fd, buffer.space, 100) > 0)
			i = 100;
	}

	config.config_mask = RTSER_SET_TIMEOUT_RX;
	config.rx_timeout = RTSER_TIMEOUT_INFINITE;
	res = ioctl(fd, RTSER_RTIOC_SET_CONFIG, &config);
#else
	nanosleep(&(struct timespec){ 0, 500000000 }, NULL);
	res = tcflush(fd, TCIOFLUSH);
	assert(res == 0);
#endif

	mip_packet_send(fd, packet, CMDSET_BASE, CMD_PING, CMD_PING_LEN, NULL);
	mip_packet_receive(fd, packet, sizeof(buffer));
	assert(mip_packet_acked(packet));

	mip_packet_send(fd, packet, CMDSET_BASE, CMD_DEVINFO, CMD_DEVINFO_LEN, NULL);
	mip_packet_receive(fd, packet, sizeof(buffer));
	assert(mip_packet_acked(packet));
	printf("Firmware version: 0x%04x\n",
	       ((int)packet->field_data[6] << 8) + packet->field_data[7]);
	str[16] = 0;
	memcpy(str, &packet->field_data[8], 16);
	printf("Model: %16s\n", str);

	mip_packet_send(fd, packet, CMDSET_3DM, CMD_AHRSFMT, CMD_AHRSFMT_LEN,
			ahrs_format);
	mip_packet_receive(fd, packet, sizeof(buffer));
	assert(mip_packet_acked(packet));


	mip_packet_send(fd, packet, CMDSET_3DM, CMD_AHRSFMT, CMD_AHRSFMT_LEN,
			ahrs_save);
	mip_packet_receive(fd, packet, sizeof(buffer));
	assert(mip_packet_acked(packet));

	mip_packet_send(fd, packet, CMDSET_3DM, CMD_CONTMODE, CMD_CONTMODE_LEN,
			ahrs_enable_cont);
	mip_packet_receive(fd, packet, sizeof(buffer));
	assert(mip_packet_acked(packet));

	clock_gettime(CLOCK_MONOTONIC, &last);

	while (1) {
		mip_packet_receive(fd, packet, sizeof(buffer));

		clock_gettime(CLOCK_MONOTONIC, &now);
		delta = (now.tv_sec - last.tv_sec) * 1000000000LL +
			now.tv_nsec - last.tv_nsec;
		last = now;

		for (i = 0; i < ARRAY_SIZE(raw_value); i++)
			raw_value[i] =
				read_sp_ieee754(&packet->field_data[i*4]);
		printf("dt: %6.03f ms, %8.3f /%8.3f /%8.3f   \r",
		       delta / 1000000.0, raw_value[0], raw_value[1],
		       raw_value[2]);
		/* Note: Drifting clocks (UHCI vs. sensor) can cause
		 * variations of +/- 1 frame. */
		if (delta > 11500000)
			printf("\nTiming violation!\n");
	}
}
