/*
 * Test for using MicroStrain 3DM-GX3-25 with RT CDC ACM driver
 *
 * Copyright (c) Siemens AG, 2012, 2013
 * Authors:
 *  Jan Kiszka <jan.kiszka@siemens.com>
 *
 * This work is licensed under the terms of the GNU GPL, version 2.  See
 * the COPYING file in the top-level directory.
 */

#include <assert.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <termio.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#ifdef __XENO__
#include <rtdm/rtserial.h>
#endif

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

#define CMD_RAW_AC_AN			0xc1
#define  CMD_RAW_AC_AN_LEN		1
#define  RPLY_RAW_AC_AN_LEN		31

#define CMD_VEC_AC_AN			0xc2
#define  CMD_VEC_AC_AN_LEN		1
#define  RPLY_VEC_AC_AN_LEN		31

#define CMD_CONT_MODE			0xc4
#define  CMD_CONT_MODE_LEN		4
#define  CONT_MODE_CONF1		0xc1
#define  CONT_MODE_CONF2		0x29
#define  RPLY_CONT_MODE_LEN		8

#define CMD_DATA_AC_AN_MA_OR		0xcc
#define  CMD_DATA_AC_AN_MA_OR_LEN	1
#define  RPLY_DATA_AC_AN_MA_OR_LEN	79

#define CMD_SAMP_SETTINGS		0xdb
#define  CMD_SAMP_SETTINGS_LEN		20
#define  SAMP_SETTINGS_CONF1		0xa8
#define  SAMP_SETTINGS_CONF2		0xb9
#define  RPLY_SAMP_SETTINGS_LEN		19

#define CMD_FW_VER			0xe9
#define  CMD_FW_VER_LEN			1
#define  RPLY_FW_VER_LEN		7

#define CMD_DEV_ID			0xea
#define  CMD_DEV_ID_LEN			2
#define  DEV_ID_MODEL_NAME		2
#define  RPLY_DEV_ID_LEN		20

#define CMD_STOP_CONT			0xfa
#define  CMD_STOP_CONT_LEN		3
#define  STOP_CONT_CONF1		0x75
#define  STOP_CONT_CONF2		0xb4

#define CMD_DEV_RESET			0xfe
#define  CMD_DEV_RESET_LEN		3
#define  DEV_RESET_CONF1		0x9e
#define  DEV_RESET_CONF2		0x3a

static bool packet_ok(const uint8_t *buf, size_t len, size_t expected_len)
{
	uint16_t csum = 0;
	off_t i;

	assert(expected_len >= 2);

	if (len != expected_len) {
		printf("invalid packet length (%zu vs. expected %zu)\n", len, 
		       expected_len);
		return false;
	}

	for (i = 0; i < len - 2; i++)
		csum += buf[i];
	if (csum >> 8 != buf[len - 2] || (uint8_t)csum != buf[len - 1]) {
		printf("invalid packet checksum (%02x", buf[0]);
		for (i = 1; i < len; i++)
			printf(" %02x", buf[i]);
		printf(")\n");
		return false;
	}

	return true;
}

static uint32_t read_be_u32(uint8_t *buf)
{
	return (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3];
}

static float read_sp_ieee754(uint8_t *buf)
{
	union {
		uint32_t binary_val;
		float float_val;
	} convert;

	convert.binary_val = *(uint32_t *)buf;
	return convert.float_val;
}

int main(int argc, char *argv[])
{
	struct sched_param param = { .sched_priority = 1 };
	struct timespec last, now;
	unsigned long long delta;
	uint8_t buffer[100];
	uint32_t version;
	float raw_value[6];
	float timer;
	int fd, res, i;
#ifdef __XENO__
	struct rtser_config config;
#endif

	res = mlockall(MCL_CURRENT | MCL_FUTURE);
	assert(res == 0);

	res = pthread_setschedparam(pthread_self(), SCHED_FIFO, &param);
	assert(res == 0);

	if (argc < 2) {
		printf("%s <device-name>\n", argv[0]);
		return 1;
	}
	fd = open(argv[1], O_RDWR);
	assert(fd >= 0);

	buffer[0] = CMD_STOP_CONT;
	buffer[1] = STOP_CONT_CONF1;
	buffer[2] = STOP_CONT_CONF2;
	res = write(fd, buffer, CMD_STOP_CONT_LEN);
	assert(res == CMD_STOP_CONT_LEN);

#ifdef __XENO__
	config.config_mask = RTSER_SET_TIMEOUT_RX;
	config.rx_timeout = RTSER_TIMEOUT_NONE;
	res = ioctl(fd, RTSER_RTIOC_SET_CONFIG, &config);
	assert(res == 0);

	i = 100;
	while (--i > 0) {
		nanosleep(&(struct timespec){ 0, 1000000 }, NULL);
		while (read(fd, buffer, 100) > 0)
			i = 100;
	}

	config.config_mask = RTSER_SET_TIMEOUT_RX;
	config.rx_timeout = RTSER_TIMEOUT_INFINITE;
	res = ioctl(fd, RTSER_RTIOC_SET_CONFIG, &config);
#else
	nanosleep(&(struct timespec){ 0, 500000000 }, NULL);
	res = tcflush(fd, TCIOFLUSH);
	assert(res == 0);
#endif

	buffer[0] = CMD_FW_VER;
	res = write(fd, buffer, CMD_FW_VER_LEN);
	assert(res == CMD_FW_VER_LEN);

	res = read(fd, buffer, RPLY_FW_VER_LEN);
	assert(packet_ok(buffer, res, RPLY_FW_VER_LEN));
	version = read_be_u32(&buffer[1]);
	printf("Firmware version: %d.%d.%d\n", version / 1000,
	       (version / 100) % 10, version % 100);

	buffer[0] = CMD_DEV_ID;
	buffer[1] = DEV_ID_MODEL_NAME;
	res = write(fd, buffer, CMD_DEV_ID_LEN);
	assert(res == CMD_DEV_ID_LEN);

	res = read(fd, buffer, RPLY_DEV_ID_LEN);
	assert(packet_ok(buffer, res, RPLY_DEV_ID_LEN));
	buffer[18] = 0;
	printf("Model: %s\n", &buffer[2]);

	buffer[0] = CMD_SAMP_SETTINGS;
	buffer[1] = SAMP_SETTINGS_CONF1;
	buffer[2] = SAMP_SETTINGS_CONF2;
	buffer[3] = 1;
	buffer[4] = 0;
	buffer[5] = 1;
	buffer[6] = 0;
	buffer[7] = 3;
	buffer[8] = 15;
	buffer[9] = 17;
	buffer[10] = 0;
	buffer[11] = 10;
	buffer[12] = 0;
	buffer[13] = 10;
	buffer[14] = 0;
	buffer[15] = 0;
	buffer[16] = 0;
	buffer[17] = 0;
	buffer[18] = 0;
	buffer[19] = 0;
	res = write(fd, buffer, CMD_SAMP_SETTINGS_LEN);
	assert(res == CMD_SAMP_SETTINGS_LEN);

	res = read(fd, buffer, RPLY_SAMP_SETTINGS_LEN);
	assert(packet_ok(buffer, res, RPLY_SAMP_SETTINGS_LEN));

	buffer[0] = CMD_CONT_MODE;
	buffer[1] = CONT_MODE_CONF1;
	buffer[2] = CONT_MODE_CONF2;
	buffer[3] = CMD_VEC_AC_AN;
	res = write(fd, buffer, CMD_CONT_MODE_LEN);
	assert(res == CMD_CONT_MODE_LEN);

	res = read(fd, buffer, RPLY_CONT_MODE_LEN);
	assert(packet_ok(buffer, res, RPLY_CONT_MODE_LEN));
	printf("Continuous mode, command=%02x, timer=%d\n", buffer[1],
	       (buffer[2] << 8) + buffer[3]);

	clock_gettime(CLOCK_MONOTONIC, &last);

	while (1) {
		res = read(fd, buffer, RPLY_VEC_AC_AN_LEN);
		assert(packet_ok(buffer, res, RPLY_VEC_AC_AN_LEN));

		clock_gettime(CLOCK_MONOTONIC, &now);
		delta = (now.tv_sec - last.tv_sec) * 1000000000LL +
			now.tv_nsec - last.tv_nsec;
		last = now;

		for (i = 0; i < ARRAY_SIZE(raw_value); i++)
			raw_value[i] = read_sp_ieee754(&buffer[2 + i*4]);
		timer = read_be_u32(&buffer[26]);
		printf("dt: %6.03f ms, %8.3f /%8.3f /%8.3f, "
		       "%8.3f /%8.3f /%8.3f, %.01f ms   \r",
		       delta / 1000000.0,
		       raw_value[0], raw_value[1], raw_value[2],
		       raw_value[3], raw_value[4], raw_value[5],
		       timer / 16000.0);
		if (delta > 1500000)
			printf("\nTiming violation!\n");
	}
}
